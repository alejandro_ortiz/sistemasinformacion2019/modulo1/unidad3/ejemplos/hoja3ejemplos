﻿DROP DATABASE IF EXISTS ejemplosprogramacion3;
CREATE DATABASE ejemplosprogramacion3;
USE ejemplosprogramacion3;

-- creacion de tablas
  CREATE TABLE alumnos(
    id int NOT NULL,
    nota1 int NOT NULL,
    nota2 int NOT NULL,
    nota3 int NOT NULL,
    nota4 int NOT NULL,
    grupo int NOT NULL,
    media varchar(255) DEFAULT NULL,
    max varchar(255) DEFAULT NULL,
    min varchar(255) DEFAULT NULL,
    moda varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
  );

  CREATE TABLE circulo(
    id int NOT NULL,
    radio int NOT NULL,
    tipo varchar(5) DEFAULT NULL,
    area varchar(255) DEFAULT NULL,
    perimetro varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
  );

  CREATE TABLE conversion(
    id int NOT NULL,
    cm double DEFAULT NULL,
    m double DEFAULT NULL,
    km double DEFAULT NULL,
    pulgadas double DEFAULT NULL,
    PRIMARY KEY (id)
  );

  CREATE TABLE cuadrados(
    id int NOT NULL,
    lado int NOT NULL,
    area varchar(255) DEFAULT NULL,
    perimetro varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
  );

  CREATE TABLE grupos(
    id int NOT NULL,
    media varchar(255) DEFAULT NULL,
    max varchar(255) DEFAULT NULL,
    min varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
  );

  CREATE TABLE rectangulo(
    id int NOT NULL,
    lado1 int NOT NULL,
    lado2 int NOT NULL,
    area varchar(255) DEFAULT NULL,
    perimetro varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
  );

  CREATE TABLE triangulos(
    id int NOT NULL,
    base int NOT NULL,
    altura int NOT NULL,
    lado2 int NOT NULL,
    lado3 int NOT NULL,
    area varchar(255) DEFAULT NULL,
    perimetro varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
  );

-- creacion de indices

  ALTER TABLE alumnos ADD INDEX grupo_index (grupo);
  ALTER TABLE alumnos ADD INDEX nota1_index (nota1);
  ALTER TABLE alumnos ADD INDEX nota2_index (nota2);
  ALTER TABLE alumnos ADD INDEX nota3_index (nota3);
  ALTER TABLE alumnos ADD INDEX nota4_index (nota4);  
  ALTER TABLE circulo ADD INDEX radio_index (radio);
  ALTER TABLE cuadrados ADD INDEX lado_index (lado);
  ALTER TABLE rectangulo ADD INDEX lado1_index (lado1);
  ALTER TABLE rectangulo ADD INDEX lado2_index (lado2);
  ALTER TABLE triangulos ADD INDEX altura_index (altura);
  ALTER TABLE triangulos ADD INDEX base_index (base);
  ALTER TABLE triangulos ADD INDEX lado2_index (lado2);
  ALTER TABLE triangulos ADD INDEX lado3_index (lado3);