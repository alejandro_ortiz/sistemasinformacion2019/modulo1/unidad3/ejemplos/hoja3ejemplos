﻿USE ejemplosprogramacion3;

-- Función área triangulo.

  DELIMITER //
  CREATE OR REPLACE FUNCTION areaTriangulo(base float, altura float)
    RETURNS float
    BEGIN
       
      DECLARE varea float DEFAULT 0;

      SET varea = (base * altura)/2;

      RETURN varea;
    END //
  DELIMITER ;

  SELECT areaTriangulo(3,4);

-- Función perímetro triangulo

  DELIMITER //
  CREATE OR REPLACE FUNCTION perimetroTriangulo(base float, lado2 float, lado3 float)
    RETURNS float
    BEGIN
      
      DECLARE vperimetro float DEFAULT 0;

      SET vperimetro = base + lado2 + lado3;

      RETURN vperimetro;
    END //
  DELIMITER ;

  SELECT perimetroTriangulo(4,7,6);

-- Procedimiento almacenado actualiza area y perimetro triangulos

  DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaTriangulos(id1 int, id2 int)
    BEGIN
      
      UPDATE triangulos SET 
        area = areaTriangulo(base, altura),
        perimetro = perimetroTriangulo(base, lado2, lado3) 
      WHERE id BETWEEN id1 AND id2;

    END //
  DELIMITER ;

  CALL actualizaTriangulos(3,10);

  SELECT * FROM triangulos t;

-- Funcion área cuadrado

  DELIMITER //
  CREATE OR REPLACE FUNCTION areaCuadado(lado float)
    RETURNS float
    BEGIN
      
      DECLARE varea float DEFAULT 0;

      SET varea = POW(lado,2);

      RETURN varea;
    END //
  DELIMITER ;

  SELECT areaCuadado(10);

-- Función perímetro cuadrado

  DELIMITER //
  CREATE OR REPLACE FUNCTION perimetroCuadrado(lado float)
    RETURNS float
    BEGIN
      
      DECLARE vperimetro float DEFAULT 0;

      SET vperimetro = lado * 4;

      RETURN vperimetro;
    END //
  DELIMITER ;

  SELECT perimetroCuadrado(22);

-- Procedimiento almacenado actualiza area y perimetro cuadrados

  DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaCuadrados(id1 int, id2 int)
    BEGIN
        
      UPDATE cuadrados SET
        area = areaCuadado(lado),
        perimetro = perimetroCuadrado(lado)
      WHERE id BETWEEN id1 AND id2;

    END //
  DELIMITER ;

  CALL actualizaCuadrados(5,17);

  SELECT * FROM cuadrados c;

-- Funcion area rectangulo

  DELIMITER //
  CREATE OR REPLACE FUNCTION areaRectangulo(lado1 float, lado2 float)
    RETURNS float
    BEGIN
      
      DECLARE varea float DEFAULT 0;

      SET varea = lado1 * lado2;

      RETURN varea;
    END //
  DELIMITER ;

  SELECT areaRectangulo(4,6);

-- Funcion perímetro rectangulo

  DELIMITER //
  CREATE OR REPLACE FUNCTION perimetroRectangulo(lado1 float, lado2 float)
    RETURNS float
    BEGIN
      
      DECLARE vperimetro float DEFAULT 0;

      SET vperimetro = (lado1 + lado2) * 2;

      RETURN vperimetro;
    END //
  DELIMITER ;

  SELECT perimetroRectangulo(5,8);

-- Procedimiento almacenado actualiza area y perimetro rectangulo

  DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaRectangulos(id1 int, id2 int)
    BEGIN

      UPDATE rectangulo set
        area = areaRectangulo(lado1, lado2),
        perimetro = perimetroRectangulo(lado1, lado2)
      WHERE id BETWEEN id1 AND id2; 
      
    END //
  DELIMITER ;

  CALL actualizaRectangulos(1,8);

  SELECT * FROM rectangulo r;

-- Funcion area circulo

  DELIMITER //
  CREATE OR REPLACE FUNCTION areaCirculo(radio float)
    RETURNS float
    BEGIN
      
      DECLARE varea float DEFAULT 0;

      SET varea =  PI()*POW(radio,2);                        

      RETURN varea;
    END //
  DELIMITER ;

  SELECT areaCirculo(4);

-- Funcion perimetro circulo

  DELIMITER //
  CREATE OR REPLACE FUNCTION perimetroCirculo(radio float)
    RETURNS float
    BEGIN
      
      DECLARE vperimetro float DEFAULT 0;

      SET vperimetro = 2 * PI() * radio;

      RETURN vperimetro;
    END //
  DELIMITER ;

  SELECT perimetroCirculo(4);

-- Procedimiento almacenado actualiza area y perimetro circulo
  
  DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaCirculos(id1 int, id2 int, tipo char(1))
    BEGIN
      
      IF (tipo IS NULL) THEN
        UPDATE circulo SET
          area = areaCirculo(radio),
          perimetro = perimetroCirculo(radio)
        WHERE id BETWEEN id1 AND id2;
      ELSE
        UPDATE circulo c SET
          area = areaCirculo(radio),
          perimetro = perimetroCirculo(radio)
        WHERE id BETWEEN id1 AND id2 
        AND c.tipo = tipo;
      END IF;

    END //
  DELIMITER ; 

  CALL actualizaCirculos(4,7,'a');
  SELECT * FROM circulo c;
  CALL actualizaCirculos(1,12,'b');
  SELECT * FROM circulo c;
  CALL actualizaCirculos(14,17,null);
  SELECT * FROM circulo c;

-- Funcion media

  DELIMITER //
  CREATE OR REPLACE FUNCTION media(nota1 float, nota2 float, nota3 float, nota4 float)
    RETURNS float
    BEGIN
      
      DECLARE vmedia float DEFAULT 0;

      SET vmedia = (nota1 + nota2 + nota3 + nota4)/4;                          

      RETURN vmedia;
    END //
  DELIMITER ;

  SELECT media(4,9,2,5);

-- Funcion minimo

  DELIMITER //
  CREATE OR REPLACE FUNCTION minimo(nota1 float, nota2 float, nota3 float, nota4 float)
    RETURNS float
    BEGIN
       
      DECLARE vminimo float DEFAULT 0;

      SET vminimo = LEAST(nota1,nota2,nota3,nota4);

      RETURN vminimo;
    END //
  DELIMITER ;

  SELECT minimo(5,7,2,9);

-- Funcion maximo

  DELIMITER //
  CREATE OR REPLACE FUNCTION maximo(nota1 float, nota2 float, nota3 float, nota4 float)
    RETURNS float
    BEGIN
      DECLARE vmaximo float DEFAULT 0;

     SET vmaximo = GREATEST(nota1,nota2,nota3,nota4);
  
      RETURN vmaximo;
    END //
  DELIMITER ;

  SELECT maximo(5,7,2,9);

-- Funcion moda
 
  DELIMITER //
  CREATE OR REPLACE FUNCTION moda(nota1 float, nota2 float, nota3 float, nota4 float)
    RETURNS float
    BEGIN
      
      DECLARE vmoda float DEFAULT 0;
      DECLARE vmaximo int DEFAULT 0;
      
      CREATE OR REPLACE TEMPORARY TABLE notas(
        id int AUTO_INCREMENT PRIMARY KEY,
        nota float
      );

      INSERT INTO notas (nota) VALUES 
        (nota1),(nota2),(nota3),(nota4);
    
      SELECT MAX(repeticiones) INTO vmaximo 
        FROM(
          SELECT nota, COUNT(*)repeticiones 
            FROM notas 
            GROUP BY nota
          )c1;
      
    
      SELECT AVG(nota) INTO vmoda 
        FROM (
          SELECT nota, COUNT(*)repeticiones 
            FROM notas 
            GROUP BY nota
          )c1
        WHERE c1.repeticiones=vmaximo;
            
      RETURN vmoda;
    END //
  DELIMITER ; 

  SELECT moda(4,7,7,4);

-- Procedimiento almacenado actualiza las notas minima, maxima, media y moda de los alumnos
-- y la nota media de los grupos

  DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaAlumnos(id1 int, id2 int)
    BEGIN
      
      UPDATE alumnos SET 
        min = minimo(nota1, nota2, nota3, nota4), 
        max = maximo(nota1, nota2, nota3, nota4),
        media = media(nota1, nota2, nota3, nota4),
        moda = moda(nota1, nota2, nota3, nota4)
      WHERE id BETWEEN id1 AND id2; 
      
      UPDATE grupos SET
        media = (SELECT AVG(media) med1 FROM alumnos 
        WHERE id BETWEEN id1 AND id2 AND grupo = 1)
      WHERE id = 1;

      UPDATE grupos SET
        media = (SELECT AVG(media) med2 FROM alumnos 
        WHERE id BETWEEN id1 AND id2 AND grupo = 2)
      WHERE id = 2;
        
    END //
  DELIMITER ;

  CALL actualizaAlumnos(1,7);
  SELECT * FROM alumnos a;
  SELECT * FROM grupos g;

-- Realizar un procedimiento almacenado que le pasas como argumento un id y te calcula la conversión
-- de unidades de todos los registros que estén detrás de ese id.
-- Si está escrito los cm calculara m, km y pulgadas. Si está escrito en m calculara cm, km y 
-- pulgadas y así consecutivamente

  DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizarConversion(id1 int)
    BEGIN

      UPDATE conversion c
        SET m = cm/100 ,
            km = cm/100000,
            pulgadas = cm/2.54
      WHERE c.id > id1 AND c.cm IS NOT NULL;
     
      UPDATE conversion c
        SET cm = m*100,
            km = m/1000,
            pulgadas = m*39.3701
      WHERE c.id > id1 AND c.m IS NOT NULL;
          
      UPDATE conversion c
        SET cm = km*100000,
            m = km*1000,
            pulgadas = km*39370.1
      WHERE c.id > id1 AND c.km IS NOT NULL;
     
      UPDATE conversion c
        SET cm = pulgadas*2.54,
            m = pulgadas/0.0254,
            km = pulgadas/39370.1
      WHERE c.id > id1 AND c.pulgadas IS NOT NULL;

    END //
  DELIMITER ;

  CALL actualizarConversion(6);

  SELECT * FROM conversion c;